import os, gzip
srcworld = "World"
targetworld = "World1"

os.mkdir(targetworld)

def base36encode(number, alphabet='0123456789abcdefghijklmnopqrstuvwxyz'):
    """Converts an integer to a base36 string."""
    base36 = ''
    sign = ''
    if number < 0:
        sign = '-'
        number = -number
    if 0 <= number < len(alphabet):
        return sign + alphabet[number]
    while number != 0:
        number, i = divmod(number, len(alphabet))
        base36 = alphabet[i] + base36
    return sign + base36

for rootfolder in os.listdir(srcworld):
	if (len(rootfolder) <= 2):
		for subfolder in os.listdir(srcworld + "/" + rootfolder):
			for chunk in os.listdir(srcworld + "/" + rootfolder + "/" + subfolder):
				print("BREAKING " + chunk)
				chunkfname = srcworld + "/" + rootfolder + "/" + subfolder + "/" + chunk
				data = bytearray(gzip.open(chunkfname, compresslevel=0).read())

				xPos = data.find(b"xPos") + 4
				zPos = data.find(b"zPos") + 4

				#print(data[xPos:xPos + 4])
				#print(data[zPos:zPos + 4])

				xPosValue = int.from_bytes(data[xPos:xPos + 4], byteorder='big')
				zPosValue = int.from_bytes(data[zPos:zPos + 4], byteorder='big')

				#print(zPosValue)
				#if (xPosValue & (1 << 31)): xPosValue = -(((~xPosValue) & 0xFFFFFFFF) + 1)
				#if (zPosValue & (1 << 31)): zPosValue = -(((~zPosValue) & 0xFFFFFFFF) + 1)

				#print("{}: {} {}".format(chunk, xPosValue, zPosValue))

				xPosValue ^= 0xDEADBEEF
				zPosValue ^= 0xDEADBEEF

				xPosValue &= 0xFFFFFFFF
				zPosValue &= 0xFFFFFFFF

				data[xPos:xPos + 4] = (xPosValue).to_bytes(4, byteorder='big')
				data[zPos:zPos + 4] = (zPosValue).to_bytes(4, byteorder='big')

				code1 = xPosValue
				if (code1 & (1 << 31) != 0):
					code1 = -((~code1 + 1) & 0xFFFFFFFF)

				code2 = zPosValue
				if (code2 & (1 << 31) != 0):
					code2 = -((~code2 + 1) & 0xFFFFFFFF)

				rootfolder2 = rootfolder
				subfolder2 = subfolder
				#rootfolder2 = base36encode(xPosValue % 64)
				#subfolder2 = base36encode(zPosValue % 64)
				chunk = "c." + base36encode(code1) + "." + base36encode(code2) + ".dat"
				#print("to " + chunk)

				if (not os.path.exists(targetworld + "/" + rootfolder2)):
					os.mkdir(targetworld + "/" + rootfolder2)

				if (not os.path.exists(targetworld + "/" + rootfolder2 + "/" + subfolder2)):
					os.mkdir(targetworld + "/" + rootfolder2 + "/" + subfolder2)

				gzip.GzipFile(targetworld + "/" + rootfolder2 + "/" + subfolder2 + "/" + chunk, "w", compresslevel=0).write(data)