import subprocess, time, random, os
import threading
from text import *

whitelist = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789()\n"

def die(timeout):
	time.sleep(timeout)
	if (not jailed):
		print("YOU HEAR THE STONE ALMOST CLOSING IN THE LAIR, SO YOUR RUSH TO THE EXIT TO GET OUT SAFELY.", flush = True)
	else:
		print("YOU HEAR THE STONE CLOSING IN THE LAIR. YOU WON'T BE ABLE TO GET OUT EVER AGAIN. GAME OVER.", flush = True)

	os._exit(0)

def manualexit():
	print("YOU DECIDE KRAMPUS IS NOT WORTH YOUR TIME AND GET OUT OF HIS LAIR TO DO OTHER CHALLS.", flush = True)
	os._exit(0)

thr = threading.Thread (target = die, args = (60 * 30,))
thr.start ()

currentRoom = 0
somethingWasUsed = False
somethingWasTaken = False
hasFireflies = False
jailed = False
jailedCharFail = 0
inventory = []

def readPromptJail():
	global jailedCharFail
	print("> ", end = "", flush = True)
	
	inp = input()
	bad = False

	for c in inp:
		if (c not in "GEMHUNTERSNARECANTIMER)(,/v".lower()):
			print(c)
			print(random.choice(badCharacter))
			bad = True
			jailedCharFail += 1
			if (jailedCharFail == 10):
				print(jailTextReminder)
				jailedCharFail = 0
				
			break

	if (not bad):
		try:
			print("You try double eval'ing your contraption: " + repr(eval(inp)), flush = True)
			eval(eval(inp))
		except:
			print(random.choice(execFail))

def readPrompt():
	global currentRoom
	global somethingWasUsed
	global somethingWasTaken
	global hasFireflies
	global jailed

	print("> ", end = "", flush = True)
	inp = input().lower().strip()
	cmd = inp.split(" ")

	if (len(inp) == 0):
		return

	if (inp == "help"):
		print(hlp)

	elif ("north" in cmd or "up" in cmd or "n" in cmd):
		nextRoom = roomConnections[currentRoom][0]
		if (nextRoom == -1): print("THERE IS A WALL STOPPING YOU FROM GOING NORTH.")
		else:
			currentRoom = nextRoom
			print("YOU GO NORTH.")

	elif ("east" in cmd or "right" in cmd or "e" in cmd):
		nextRoom = roomConnections[currentRoom][1]
		if (nextRoom == -1): print("THERE IS A WALL STOPPING YOU FROM GOING EAST.")
		else:
			currentRoom = nextRoom
			print("YOU GO EAST.")

	elif ("south" in cmd or "down" in cmd or "s" in cmd):
		if (currentRoom == 0):
			manualexit()

		nextRoom = roomConnections[currentRoom][2]
		if (nextRoom == -1): print("THERE IS A WALL STOPPING YOU FROM GOING SOUTH.")
		else:
			currentRoom = nextRoom
			print("YOU GO SOUTH.")

	elif ("west" in cmd or "left" in cmd or "w" in cmd):
		nextRoom = roomConnections[currentRoom][3]
		if (nextRoom == -1): print("THERE IS A WALL STOPPING YOU FROM GOING WEST.")
		else:
			currentRoom = nextRoom
			print("YOU GO WEST.")

	elif (cmd[0] == "go"):
		print("GO WHERE? NORTH, EAST, SOUTH, WEST.")

	elif (cmd[0] == "exit" or cmd[0] == "quit"):
		manualexit()

	elif (cmd[0] == "look" or cmd[0] == "see"):
		print(roomLook[currentRoom][roomState[currentRoom]])

	elif (cmd[0] == "take"):
		if (len(cmd) == 1): return print("TAKE WHAT?")

		somethingWasTaken = False

		if (currentRoom == 1):
			if ("can" in cmd): take("can", 0)
			if ("bow" in cmd or "arrow" in cmd or "arrows" in cmd): take("bow", 2)

		elif (currentRoom == 2):
			if ("timer" in cmd or "kitchen" in cmd): take("timer", 1)

		elif (currentRoom == 4):
			if ("hunter" in cmd or "hunter's" in cmd or "snare" in cmd): take("hunter", 1)
			if (roomState[currentRoom] == 0 and "fireflies" in cmd):
				print("YOU CAN'T TAKE THE FIREFLIES WITH YOUR HAND")
				somethingWasTaken = True

		elif (currentRoom == 7):
			if ("gem" in cmd): take("gem", 0)
		
		if (not somethingWasTaken):
			print("YOU CAN'T TAKE THAT.")

	elif (cmd[0] == "use"):
		if (len(cmd) == 1): return print("USE WHAT?")

		somethingWasUsed = False

		if (currentRoom == 1):
			if ("can" in cmd):
				if (hasFireflies and "can" in inventory):
					print("YOU FREE THE FIREFLIES FROM THE CAN.")
					hasFireflies = False

				use("can", 1)

		elif (currentRoom == 2):
			if ("can" in cmd):
				if (hasFireflies and "can" in inventory):
					use("can", 0)
					hasFireflies = False

		elif (currentRoom == 4):
			if ("can" in cmd):
				oldRoomState = roomState[currentRoom]
				use("can", -1)
				if (oldRoomState > 1):
					roomState[currentRoom] = oldRoomState

				if (somethingWasUsed):
					hasFireflies = True
		else:
			if ("can" in cmd and hasFireflies):
				print("YOU FREE THE FIREFLIES FROM THE CAN.")
				print("THE ROOM WAS ALREADY LIT UP, SO YOU DON'T SEE ANYTHING NEW.")
				hasFireflies = False
				somethingWasUsed = True

		if (not somethingWasUsed):
			print("YOU CAN'T USE THAT.")

	elif (cmd[0] == "read"):
		if (len(cmd) == 1): return print("READ WHAT?")

		somethingWasRead = False
		if (currentRoom == 5 or currentRoom == 6):
			if ("sign" in cmd):
				somethingWasRead = True
				print(readText[currentRoom]["sign"])

		if (not somethingWasRead):
			print("YOU CAN'T READ THAT.")

	else:
		print(random.choice(unknownCommand))
		print("YOU CAN LOOK, READ, TAKE OR USE.")

	if (currentRoom == 3 and len(inventory) == 5 and roomState[currentRoom] == 0):
		roomState[currentRoom] += 1
		jailed = True
		print(roomLook[currentRoom][roomState[currentRoom]])
		print(jailText)

def take(obj, stateRequired):
	global roomState
	global somethingWasTaken
	if (roomState[currentRoom] == stateRequired):
		print(takeText[currentRoom][obj])
		inventory.append(obj)
		somethingWasTaken = True
		roomState[currentRoom] += 1

def use(obj, stateRequired):
	global roomState
	global somethingWasUsed
	if (obj in inventory and (stateRequired == -1 or roomState[currentRoom] == stateRequired)):
		print(useText[currentRoom][obj])
		somethingWasUsed = True
		roomState[currentRoom] += 1

print("""

 
                    /   \\              /'\\       _                              
\\_..           /'.,/     \\_         .,'   \\     / \\_                            
    \\         /            \\      _/       \\_  /    \\     _                     
     \\__,.   /              \\    /           \\/.,   _|  _/ \\                    
          \\_/                \\  /',.,''\\      \\_ \\_/  \\/    \\                   
                           _  \\/   /    ',../',.\\    _/      \\                  
             /           _/m\\  \\  /    |         \\  /.,/'\\   _\\                 
           _/           /MMmm\\  \\_     |          \\/      \\_/  \\                
          /      \\     |MMMMmm|   \\__   \\          \\_       \\   \\_              
                  \\   /MMMMMMm|      \\   \\           \\       \\    \\             
                   \\  |MMMMMMmm\\      \\___            \\_      \\_   \\            
                    \\|MMMMMMMMmm|____.'  /\\_            \\       \\   \\_          
                    /'.,___________...,,'   \\            \\   \\        \\         
                   /       \\          |      \\    |__     \\   \\_       \\        
                 _/        |           \\      \\_     \\     \\    \\       \\_      
                /                               \\     \\     \\_   \\        \\     
                                                 \\     \\      \\   \\__      \\    
                                                  \\     \\_     \\     \\      \\   
                                                   |      \\     \\     \\      \\  
                                                    \\ms          |            \\ 
""")

print ("░▒▓█ YOU ARRIVED AT KRAMPUS'S LAIR. █▓▒░")
print ("YOU HEAR A LARGE STONE ROLLING AT THE ENTRANCE.")
print ("JUDGING BY ITS SPEED, YOU HAVE ABOUT HALF AN HOUR TILL THE LAIR GETS SEALED SHUT, LEAVING YOU WITH NO ESCAPE.", flush = True)

while True:
	if (jailed):
		readPromptJail()
	else:
		readPrompt()