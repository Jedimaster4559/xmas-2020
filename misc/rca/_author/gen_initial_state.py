from characters import *
import random

matrix = [[0 for _ in range(70)] for __ in range(70)]

def printMatrix():
	global matrix
	for l in matrix:
		for i in l:
			if i == 1:
				print("#", end="")
			else:
				print(" ", end="")
		print()


def putCharsOnLine(line, chrs):
	global matrix
	y = (line - 1) * 10
	y_for_char = 0

	for y in range(y, y + 8):
		x = 0
		for char_to_write in chrs:
			for bit in char_to_write[y_for_char]:
				matrix[y][x] = bit
				x += 1
		y_for_char += 1


def dumpMatrix():
	global matrix
	d = ""
	for l in matrix:
		for i in l:
			if i == 1:
				d += "1"
			else:
				d += "0"
	h = hex(int(d, 2))[2:]
	if len(h) != len(matrix) ** 2:
		return (len(matrix) ** 2 - len(h) * 4) // 4 * "0" + h
	return h


def main():
	putCharsOnLine(1, [x, minus, m, a, s, deschide_acolada, a])
	putCharsOnLine(2, [underscore, s, m, a, l, l, underscore])
	putCharsOnLine(3, [w, o, r, l, d, underscore, i])
	putCharsOnLine(4, [n, s, i, d, e, underscore, y])
	putCharsOnLine(5, [o, u, r, underscore, b, r, o])
	putCharsOnLine(6, [w, s, e, r, inchide_acolada, blank, blank])
	printMatrix()
	print(dumpMatrix())


if __name__ == "__main__":
	main()
