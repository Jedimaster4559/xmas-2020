noSquares = 70
start = 160760000
matrix = [[0 for _ in range(noSquares)] for __ in range(noSquares)]

def unpack(h):
	a = bin(int(h, 16))[2:]
	if len(a) < noSquares * noSquares:
		a = "0" * (noSquares ** 2 - len(a)) + a
	return [int(i) for i in a]


def getCell(x, y):
	global matrix, noSquares
	if x < 0:
		x = noSquares - 1
	if x >= noSquares:
		x = 0
	if y < 0:
		y = noSquares - 1
	if y >= noSquares:
		y = 0
	return matrix[y][x];


def putCell(x, y, value):
	global matrix, noSquares
	if x < 0:
		x = noSquares - 1
	if x >= noSquares:
		x = 0
	if y < 0:
		y = noSquares - 1
	if y >= noSquares:
		y = 0
	matrix[y][x] = value;


def getSquare(x, y):
	return [getCell(x, y), getCell(x + 1, y), getCell(x, y + 1), getCell(x + 1, y + 1)]


def putSquare(x, y, vals):
	putCell(x, y, vals[0])
	putCell(x + 1, y, vals[1])
	putCell(x, y + 1, vals[2])
	putCell(x + 1, y + 1, vals[3])


def reverseSquare(x, y):
	square = getSquare(x, y)
	noLiveCells = sum(square)
	if noLiveCells == 1:
		newSquare = [0, 0, 0, 0]
		newSquare[3] = square[0]
		newSquare[2] = square[1]
		newSquare[1] = square[2]
		newSquare[0] = square[3]
		square = newSquare;
	if noLiveCells != 2:
		for i, v in enumerate(square):
			if v == 1:
				square[i] = 0
			else:
				square[i] = 1
	return square


def prevMatrix(m, roundNo, silent = True):
	global noSquares
	startCoord = -(roundNo % 2)
	if not silent:
		print(f"Round {roundNo}")

	for i in range(startCoord, noSquares, 2):
		for j in range(startCoord, noSquares, 2):
			putSquare(i, j, reverseSquare(i, j));
	return m


def processSquare(x, y):
	square = getSquare(x, y)
	noLiveCells = sum(square);
	if noLiveCells != 2:
		for i in range(len(square)):
			if square[i] == 1:
				square[i] = 0
			else:
				square[i] = 1
	if noLiveCells == 3:
		newSquare = [0, 0, 0, 0]
		newSquare[0] = square[3]
		newSquare[1] = square[2]
		newSquare[2] = square[1]
		newSquare[3] = square[0]
		square = newSquare
	return square


def nextMatrix(m, roundNo):
	startCoord = -(roundNo % 2);

	for i in range(startCoord, noSquares, 2):
		for j in range(startCoord, noSquares, 2):
			putSquare(i, j, processSquare(i, j));
	return m


def restoreMatrix():
	global matrix, noSquares
	initialState_packed = "44f694c51bd68cd06d977e67ab21311db80481a9da4a2dc022bbf1373532586444999029d7a516e183a2ab80bb3432382d4c713538e53ce1950b85c0b038d129ac9a503dca2fc015e3086aa1c129e911bb88a6a1116b41535761800c5a0c14ab9f7e18b0c511c456d6ae7950189bd9086a3c13820350750c8d7c7a6f98a00c3665840517c4a49c992604d4abccb3ea05008681b126fe6b802c012251ddf68b19c295d4838868c7215c092dd55c084870e4461210045427cd8f0a71108840801f42dab83480111899caf7b81ac8c0c000217d69043a0670000dc05b409d84a4881a056a2c562c15880b3008edc306d8044881b84192157b57704820003a2b3d4068a544022970cb628526850500cc4cd1600d04003082a642fad829c4198b13f3d5206918125004502c985160001002014018fc68301a0213c40a4ac7fbe04670025a02a868061124180eb044ed622604e00810dc07efd8c002044308384dc02824e9162801e84aa0111880940996f307ae62360060ba64ede92b642920010228098a4ce910944182a04ffe670970120921d1a0b7200c3194502ca26330d9c041300484df3b525ac906221a2e1ebdf10248808eed6033699e2142040db94a9695a024929a20218f512455008da200e85c86452a704a90f639c03d88820408242d34a2d4ad0558829a83d3a2881dc7224d2e62801bc1452028728da094c353276753b06e681fa8120e89842217dfef660254b1d1fb8c2fd0953ea02de6fad34e64a234634401c6bb2a986ca05df4047c34bbb84d7878ae566181134b6db583ceec955a191e36a2f156f248ab39d620d70c90086aa9b738907e93b9e191b01dc7855c680ca09"
	initialState = unpack(initialState_packed)
	for i in range(noSquares):
		for j in range(noSquares):
			matrix[i][j] = initialState[i * noSquares + j]


def printMatrix():
	global matrix
	for l in matrix:
		for i in l:
			if i == 1:
				print("#", end="")
			else:
				print(" ", end="")
		print()


def dumpMatrix():
	global matrix
	d = ""
	for l in matrix:
		for i in l:
			if i == 1:
				d += "1"
			else:
				d += "0"
	return hex(int(d, 2))[2:]


def main():
	global matrix, start
	restoreMatrix()
	for i in range(start + 1, 160760160 + 1):
		matrix = prevMatrix(matrix, 160760160 + 1 - i)
	printMatrix()


if __name__ == "__main__":
	main()
