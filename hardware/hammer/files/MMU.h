#ifndef MMU_H
#define MMU_H
#include <stdint.h>
#include "Typedefs.h"

class MMU {
	public:
		MMU(u16 _pageStart, u16 _pageEnd);
		/*
			Memory Layout
			ROM:			0x0000 - 0x1FFF
			RAM:			0x2000 - 0x23FF
			VRAM:			0x2400 - 0x3FFF
			RAMMirror:	0x4000 - 0x43FF
		*/
		
		u8 GetByteAt(u16 addr);
		u16 GetWordAt(u16 addr);

 		void SetByteAt(u16 addr, u8 value);
 		void SetWordAt(u16 addr, u16 value);

 		void LoadInMemory(u8* buffer, u16 addr, u16 size);

		u8 forcePrivileged;
		u8 isPrivileged;
		u8* MemoryMap[0x10000];

		u16 pageStart;
		u16 pageEnd;

		void CheckBitFlip(u16 addr);
	private:
		u8 Memory[0x10000]; // 64K
		u8 JunkValue;
		u32 bitflipCounter = 0;

		u16 lastRow = 0;
		u16 lastRow2 = 0;
		u16 lastRow3 = 0;

		u32 memaccessCounter = 0;
		u8 isValidAddress(u16 addr);
		void InduceBitFlip();
};

#endif