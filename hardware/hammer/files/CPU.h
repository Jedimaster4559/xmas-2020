#ifndef CPU_H
#define CPU_H

#include <stdint.h>
#include <stdio.h>
#include "Typedefs.h"
#include "MMU.h"

class CPU {
	public:
		CPU (MMU* _mmu);
		void Clock ();
		void Interrupt (uint8_t ID);
	
		// Status
		uint8_t InterruptsEnabled;
		uint8_t Halt;
		uint64_t ClockCount;
		uint64_t InstructionCount;

		// Functions - Debugging
		void Debug ();
	private:
		MMU* mmu;
		
		// Ports + SpaceInvaders custom hardware
		uint16_t reg_SHIFT;
		uint8_t ShiftOffset;
	
		// Temp Vars
		uint8_t BogusRegM;
		uint16_t WorkValue;
		
		// Registers
		uint8_t* reg_M;
		uint8_t true_reg_A;
		uint8_t* reg_A = &true_reg_A; // Pointer uniformity
	
		uint16_t reg_BC;
		uint8_t* reg_B = ((uint8_t*) &reg_BC) + 1;
		uint8_t* reg_C = ((uint8_t*) &reg_BC);
		uint16_t reg_DE;
		uint8_t* reg_D = ((uint8_t*) &reg_DE) + 1;
		uint8_t* reg_E = ((uint8_t*) &reg_DE);
		uint16_t reg_HL;
		uint8_t* reg_H = ((uint8_t*) &reg_HL) + 1;
		uint8_t* reg_L = ((uint8_t*) &reg_HL);
	
		uint16_t PC, SP;
	
		// Flags
		uint8_t FlagZSP[0x100]; // Precalculated ZSP
		uint8_t flag_Z; // Zero
		uint8_t flag_S; // Sign
		uint8_t flag_P; // Parity
		uint8_t flag_C; // Carry
		uint8_t flag_AC; // Auxiliar Carry
	
		// Functions - Control
		void Syscall (uint8_t ID);
		void Execute (uint8_t Instruction);
		
		// Functions - Memory Management
		void PushPSW ();
		void PopPSW ();
	
		// Functions - Stack Management
		uint16_t StackPop ();
		void StackPush (uint16_t Value);

		// Functions - Flags Management
		void SetFlagsAdd (uint8_t OpA, uint8_t OpB, uint8_t Carry, uint8_t CarryMode);
		void SetFlagsSub (uint8_t OpA, uint8_t OpB, uint8_t Carry, uint8_t CarryMode);
		void SetZSP (uint8_t Value);
};

#endif