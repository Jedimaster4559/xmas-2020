import time

flag = "X-MAS{S1D3CH4NN3LZ?wtf!!}"

print("""
     _________
    / ======= \\
   / __________\\
  | ___________ |
  | | -       | |
  | |         | |
  | |_________| |_________
  \\=____________/         )
  / \"\"\"\"\"\"\"\"\"\"\" \\        /
 / ::::::::::::: \\   /T\\'
(_________________)  \\_/

HELLO SANTA.
PLEASE INPUT PASSWORD:""", flush = True)

pwd = input()
print("CHECKING... PLEASE WAIT.", flush = True)
bad = False

for c in range(len(pwd)):
	if (flag[c] != pwd[c]):
		bad = True
		break

	time.sleep(0.25)

if (len(flag) != len(pwd)):
  bad = True

if (bad):
	print("PASSWORD REJECTED.", flush = True)
else:
	print("""PASSWORD ACCEPTED. WELCOME snt!

>>   Today's stats    <<
Calls .............. 1
Posts .............. 0
Email .............. 0
Naughty children ... 2
Feedback ........... 0
Lazy gnomes ........ 23
Megs Free .......... 180
>> ------------------ <<

SYSTEM EXIT.
""")