// Note: Edit in WordPad with word wrap enabled

char ScrollText[]= {
"                                        " // Filler

"Ho! ho! ho! Greetings you curious elf! This Sony PlayStation 1 Compact-Disk features a handful of fancy-schmancy animations for your eyeballs to consume! You can run this on your home television and stare at the mesmerizing background plasma for hours. Or rather not, since perhaps you have a CTF to attend... Oh well, such is life, being a little elf running around, hacking things, breaking things, maybe building them back after you broke them... It's the cycle of hacking. Ho, Ho! If only you knew my secret combination code, you could unlock powerful knowledge! If only, Ho ho! And now, a message from the original author of this rom, for posterity:"

"        " // Line break

"Greetings PSX-DEV members! Meido-Tek Productions (or just me, LameGuy64) brings you a simple little demo for the PlayStation to proudly announce that we can now develop stuff for the said system making it our first ever home console to develop homebrew software on and as a little entry for the PSXDEV CodeBlast '13 competition for fun. Also, this is my first program to be written in C++ as I've been programming in BASIC like compilers in the past 4 years but I'd still stick to BASIC though...For programming stuff for the PC of course."

"        " // Line break
"I apologize about the lousy PAL compatibility mode in this demo to those people who only have a PAL PlayStation. Its because I can't seem to move the screen display area down so I can properly center the screen...Well, this is my first PSX homebrew after all so don't expect everything to be all professionally coded."

"        " // Line break
"It took me a week to get the hang of programming in C++ and figuring out how to use the necessary libraries during the creation of this demo with the help of documentation, sample files, and the PSX-DEV forums which is pretty fast for someone who's been programming in BASIC in the past 4 years. Now that I already had a good start with PSX homebrew development, I'm pretty much ready to make a simple 2D game for the system but I'm busy working on other non-PSX related projects...Hopefully, I'm planning to make a Chip's Challenge clone with a built-in level editor for the PSX once I'm done finishing my other projects."

"        " // Line break
"I think this is the first PSX demo to demonstrate real-time plasma but I'm not really sure about it as I haven't seen or heard of one so far. The plasma routine is from one of RELsoft's fixed-point plasma samples written in QBasic which I ported to C++ for this demo. Since the routine itself is written in pure C++ code, I had to reduce its render resolution from 160x120 down to 80x60 in order for it to run at 60FPS on the PSX but it still looks cool though...It can be ported to ASM so it'll run at a full screen resolution while maintaining a constant 60FPS frame rate but I don't know much about ASM other than writing code that crashes the system. :) The scaling is done by uploading the rendered plasma image into the texture framebuffer on every screen update which is then scaled and drawn onto the screen with GsSortSprite...The whole thing appears to be very CPU intensive on the real PSX that drawing one more big sprite might result to a noticable frame rate drop."

"        " // Line break
"The absolutely adorable little girl you see on the right hand side of the screen is drawn by my good friend in DeviantART; Pegawolf. I featured it here and gave it a nice little blinking animation because I love her works and to make this demo look nicer...If you want to check out Pegawolf's other drawings, the link to her DevianART account is listed in the end of this scroll text...Originally, its supposed to be one of my characters dressed in a maid outfit while holding a floppy disk to go along with the logo but I haven't drawn a 'maided' version of the logo yet and I wanted to impress Pegawolf...And I may need a better font for this demo as well."

"        " // Line break
"Now for the greets:"
"        " // Line break
"Shadow"
"        " // Line break
"T0rxe"
"        " // Line break
"inc^lightforce"
"        " // Line break
"Orion_"
"        " // Line break
"And everyone else in the PSX-DEV forums!"

"        " // Line break
"Links:"
"        " // Line break
"Pegawolf: http://pegawolf.deviantart.com/"

"        " // Line break
"Now comes the time to wrap things up so see ya' until the next demo...That's all there is, there isn't anymore. Ho! ho! ho!"

"                                        <" // End of scrolltext line
};