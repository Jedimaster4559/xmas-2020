# X-MAS CTF 2020 Challenge repository

## Repo Main Structure:
Category > Challenge Name

## How/Where to upload a challenge:

* Add your challenge to the [sheet](https://docs.google.com/spreadsheets/d/10JjilCTJPYR52VJByehWgRuyM_QBAH0juBM6zWU_DsI/edit#gid=0). (title, category, author, description, etc.)
* Upload your public files to GDrive, create a public link and add it to the sheet (files column): `<filename>: <link>`.
* Set your resource requirements on the sheet (microbot column) like this: `cpu=0.1 mem=100`.
* Place your challenge setup in `<category>/<challenge name>`. You should have a `Dockerfile` or a `build_docker.sh` script in challenge's root folder.
* Challenge name must be the same as on the sheet, but with lowercase ascii only. Helper: `lambda s = ''.join([c for c in s.lower() if c in string.ascii_lowercase])`
