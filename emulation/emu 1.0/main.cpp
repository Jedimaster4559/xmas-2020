#include "utils.h"
#include "cpu.cpp"
#include "gpu.cpp"

u32 loadFile(u8* &buffer, const char* filename);

int main(int argc, char* argv[]) {
	if (SDL_Init (SDL_INIT_EVERYTHING) < 0) {
		printf("SDL failed to initialize: %s\n", SDL_GetError());
		return 1;
	}

	if (argc == 1) {
		printf("Usage: %s %s\n", argv[0], "file.rom");
		return 1;
	}

	u8* tape;
	u32 tapeSize = loadFile(tape, argv[1]);

	GPU gpu;
	CPU cpu(&gpu, tape, tapeSize);

	while (!cpu.halted) {
		cpu.clock();
	}

	printf("CPU halted.\n");
}

u32 loadFile(u8* &buffer, const char* filename) {
	FILE* filePointer = fopen(filename, "rb");
	if (!filePointer) {
		printf("Cannot open file: %s\n", SDL_GetError());
		return 0;
	}

	fseek(filePointer, 0L, SEEK_END);
	u32 fileSize = ftell(filePointer);

	buffer = new u8[fileSize + 1];

	rewind(filePointer);
	fread(buffer, 1, fileSize, filePointer);
	buffer[fileSize] = '\0';

	return fileSize;
}