from random import SystemRandom

class test_generator:
    def __init__(self, maxval):
        self.prime = []
        self.sieve = [1 for i in range(maxval)]
        self.rnd = SystemRandom()

        self.sieve[0] = 0
        self.sieve[1] = 0
        for i in range(2, maxval):
            if self.sieve[i] == 1:
                self.prime.append(i)
                for j in range(2 * i, maxval, i):
                    self.sieve[j] = 0

    def get_test(self, factors):
        p = self.rnd.sample(self.prime, factors)
        e1 = [self.rnd.randint(0, 3) for i in range(factors)]
        e2 = [max(self.rnd.randint(1, 4), e1[i]) for i in range(factors)]
        ans = 1
        g = 1
        l = 1
        for i in range(factors):
            g *= p[i] ** e1[i]
            l *= p[i] ** e2[i]
            if e1[i] != e2[i]:
                ans *= 2
        return g, l, ans
