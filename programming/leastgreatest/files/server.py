import os
import sys
from text import *
from test_gen import *
import threading
import time

def die(timeout):
    time.sleep(timeout)
    os._exit(0)

def is_number(x):
    for c in x:
        if not c in "0123456789":
            return False
    return True

max_time = 90
test_cnt = 100
generator = test_generator(10**6)
print (intro.format(test_cnt, max_time))

thr = threading.Thread (target = die, args = (max_time,))
thr.start ()

for i in range(test_cnt):
        print (test_no.format(i + 1, test_cnt))
        g, l, ans = generator.get_test(2 + i // 2)
        print (present_test.format(g, l))
        x = input()

        if not is_number(x):
            print (bad_input)
            exit()

        x = int(x)

        if x != ans:
            print (wrong_answer)
            exit()
        else:
            print (good_answer)

print (win.format(FLAG))
sys.stdout.flush()
