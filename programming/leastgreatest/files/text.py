test_no = "Test number: {}/{}"

wrong_answer = "That is not the correct answer!"

good_answer = "Good, that's right!"

intro = "Hey, you there! You look like you know your way with complex alogrithms.\nThere's this weird task that I can't get my head around. It goes something like this:\nGiven two numbers g and l, tell me how many pairs of numbers (x, y) exist such that gcd(x, y) = g and lcm(x, y) = l\nAlso, i have to answer {} such questions in at most {} seconds.\n"

bad_input = "Bad input, I was expecting an integer. Aborting!"

present_test = "gcd(x, y) = {}\nlcm(x, y) = {}"

win = "Wow, you really know this kind of weird math?\n Here's your flag: {}"

FLAG = "X-MAS{gr347es7_c0mm0n_d1v1s0r_4nd_l345t_c0mmon_mult1pl3_4r3_1n73rc0nn3ct3d}"
