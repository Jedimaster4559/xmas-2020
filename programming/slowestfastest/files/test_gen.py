from random import SystemRandom
from fractions import gcd

def greedy(v, sub, diff, maxSteps, totalSteps):
    for val in v:
        val -= sub
        if val <= 0:
            continue
        val = val // diff + (val % diff != 0)
        if val > maxSteps:
            return False
        totalSteps -= val
        if totalSteps < 0:
            return False
    return True

def binarySearch(v, n, k, x, y):
    maxval = max(v)
    left, right, last = 0, maxval, maxval
    while left <= right:
        mid = (left + right) // 2
        if greedy(v, mid * x, y - x, mid, mid * (n - k)):
            last = mid
            right = mid - 1
        else:
            left = mid + 1
    return last

def solve(n, k, x, y, v):
    if x > y:
        x, y, k = y, x, n - k
    if x == y:
        return max([a // x + (a % x != 0) for a in v])
    return binarySearch(v, n, k, x, y)

class lcg:
    def __init__(self, rndgen, modulo, x0 = None, a = None, c = None):
        if x0 == None:
            x0 = rndgen.randint(2, modulo - 1)
        
        if a == None:
            a = rndgen.randint(2, modulo - 1)
            while gcd(a, modulo) != 1:
                a = rndgen.randint(2, modulo - 1)
        
        if c == None:
            c = rndgen.randint(2, modulo - 1)
            while gcd(c, modulo) != 1:
                c = rndgen.randint(2, modulo - 1)
        
        self.a = a
        self.c = c
        self.mod = modulo
        self.x0 = x0
        self.x = x0

    def next(self):
        o = self.x
        self.x = (self.a * self.x + self.c) % self.mod
        return o


class test_generator:
    def __init__(self):
        self.rnd = SystemRandom()

    def get_test(self, maxsize, maxval):
        n = self.rnd.randint(3 * maxsize // 4, maxsize)
        k = self.rnd.randint(n // 8, 7 * n // 8)
        p = self.rnd.randint(int(maxval**0.5) // 4, 3 * int(maxval**0.5) // 4)
        q = self.rnd.randint(int(maxval**0.5) // 4, 3 * int(maxval**0.5) // 4)
        gen = lcg(self.rnd, maxval + 1)
        arr = []
        for i in range(n):
            arr.append(gen.next())
        ans = solve(n, k, p, q, arr)
        return gen.x0, gen.a, gen.c, gen.mod, n, k, p, q, ans
