from random import SystemRandom

def matmul(a, b, mod):
    n = len(a)
    m = len(a[0])
    p = len(b[0])
    c = [[0 for i in range(m)] for j in range(p)]
    for i in range(n):
        for j in range(p):
            for k in range(m):
                c[i][j] = (c[i][j] + a[i][k] * b[k][j]) % mod
    return c

def matpow(m, d, mod):
    if d == 1:
        return m
    r = matpow(m, d // 2, mod)
    r = matmul(r, r, mod)
    if(d & 1 == 1):
        return matmul(r, m, mod)
    return r

def solve(n, m, bl, d, mod):
    for b in bl:
        for i in range(1, n + 1):
            m[i - 1][b - 1] = 0
            m[b - 1][i - 1] = 0
    m = matpow(m, d, mod)
    return m[0][n - 1]

class test_generator:
    def __init__(self, modulo):
        self.rnd = SystemRandom()
        self.modulo = modulo

    def get_test(self, maxsize, maxblockers, maxdistance):
        size = self.rnd.randint(3 * maxsize // 4, maxsize)
        blockers = self.rnd.randint(maxblockers // 2, maxblockers)
        distance = self.rnd.randint(maxdistance // 2, maxdistance)
        mat = [[0 for i in range(size)] for j in range(size)]
        block = []
        edges = int(size ** 1.5)
        for edge in range(edges):
            while True:
                i = self.rnd.randint(1, size)
                j = self.rnd.randint(1, size)
                if i == j:
                    continue
                mat[i - 1][j - 1] = 1
                mat[j - 1][i - 1] = 1
                break
        for blocker in range(blockers):
            while 1:
                b = self.rnd.randint(2, size - 1)
                if not b in block:
                    block.append(b)
                    break

        ans = solve(size, mat, block, distance, self.modulo)
        return size, mat, block, distance, ans
