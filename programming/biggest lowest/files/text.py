test_no = "Test number: {}/{}"

wrong_answer = "That is not the correct answer!"

good_answer = "Good, that's right!"

intro = "So you think you have what it takes to be a good programmer?\nThen solve this super hardcore task:\nGiven an array print the first k1 smallest elements of the array in increasing order and then the first k2 elements of the array in decreasing order.\nYou have {} tests that you'll gave to answer in maximum {} seconds, GO!\nHere's an example of the format in which a response should be provided:\n1, 2, 3; 10, 9, 8\n"

bad_input = "Bad input, I was expecting an integer. Aborting!"

present_test = "array = {}\nk1 = {}\nk2 = {}"

win = "Wow, you really know this kind of weird math?\n Here's your flag: {}"

FLAG = "X-MAS{th15_i5_4_h34p_pr0bl3m_bu7_17'5_n0t_4_pwn_ch41l}"
